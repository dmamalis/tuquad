/*  
 *  FPGA side
 *
 *	
 *	Kimon Tsitsikas & Dimitris Mamalis
 *	
 *			   
 *--------------------------------------------------------------------------
 */

/************************************************************************
*	IMPORTANT NOTICE						*
*	----------------						*
*	In order to make these values accessible to the			*
*	interrupt handler we extended and remapped the 			*
*	ae[] array. We use the ae_sp[] array as the one			*
*	which includes the rotor values.				*
*************************************************************************/
/*	ae[0] : lift
	ae[1] : roll
	ae[2] : pitch
	ae[3] : yaw
	ae[4] : p
	ae[5] : p1
	ae[6] : p2
	ae[7] : mode
	ae[8] : sax0;
	ae[9] : say0;
	ae[10] : sp0;
	ae[11] : sq0;
	ae[12] : sr0;
	ae[13] : p_b;
	ae[14] : q_b;
	ae_sp[0] : rotor1
	ae_sp[1] : rotor2
	ae_sp[2] : rotor3
	ae_sp[3] : rotor4
 */

#include <stdio.h>
#include <string.h>

#include "./lib/common.h"

#include "assert.h"
#include "./lib/main.h"
#include "./lib/crc.h"
#include "./lib/control_qr.h"

// #define MYCLOCK (CLOCKS_PER_MS/1000)


/*------------------------------------------------------------------
 * main -- Main loop on the QR
 *------------------------------------------------------------------
 */
int main() 
{
	int j=0,i=10,v, int_c,time_divider = 0;
	int ae0,ae1,ae2,ae3,ppp;	
	unsigned char rem2pc;
	unsigned char *es_ram;
	int system_time;
	int truemode;
	//int previous = 0;
	//int clock_start;
	
	//timing variables
	int t_loop_start=0;
	int t_loop_end=0; 
	int t_resp_start=0;
	int t_resp_end=0;  
	int flag_first=0;
	int ready_input,ready_output;

	Flightvals *input= (Flightvals *)malloc(sizeof(Flightvals));
	es_ram = (unsigned char *) malloc(DL_CAPACITY*sizeof(unsigned char));

	// initialize certain variables
	frameflag = FRAMESTART;
    	iptr = optr = 0;
	X32_leds = 0;
	demo_done = 0;
	
	input->mode  = 0;
	input->lift  = 0;
	input->roll  = 0;
	input->pitch = 0;
	input->yaw   = 0;
	input->mode  = 0;
	input->p  = 0;
	input->p1  = 0;
	input->p2  = 0;

	// Initializing vector values 
	for (i=0; i<9; i++){
		new_values[i] = 0;
		crc_values[i] = 0;
		crc_values2[i] = 0;
	}
	
	ae[0] = ae[1] = ae[2] = ae[3] = ae[4] = ae[5] = ae[6] = ae[7] = ae[8] = ae[9] = ae[10] = ae[11] = ae[12] = ae[13] = ae[14] = 0;
	
	for (i=0; i<DL_CAPACITY; i++){
        	es_ram[i] = 0;
	} 
	i = 10;

	// prepare QR rx interrupt handler
        SET_INTERRUPT_VECTOR(INTERRUPT_XUFO, &isr_qr_link);
        SET_INTERRUPT_PRIORITY(INTERRUPT_XUFO, 21);
	isr_qr_counter = isr_qr_time = 0;
	
        ENABLE_INTERRUPT(INTERRUPT_XUFO);
 	
	// prepare timer interrupt
        // X32_timer_per =  799 * MYCLOCK;
        // SET_INTERRUPT_VECTOR(INTERRUPT_TIMER1, &isr_qr_link);
        // SET_INTERRUPT_PRIORITY(INTERRUPT_TIMER1, 22);
        // ENABLE_INTERRUPT(INTERRUPT_TIMER1);
	
	// prepare rs232 rx interrupt and getchar handler
        SET_INTERRUPT_VECTOR(INTERRUPT_PRIMARY_RX, &isr_rs232_rx);
        SET_INTERRUPT_PRIORITY(INTERRUPT_PRIMARY_RX, 20);
	while (X32_rs232_char) c = X32_rs232_data; // empty buffer
        ENABLE_INTERRUPT(INTERRUPT_PRIMARY_RX);

        // prepare wireless rx interrupt and getchar handler
        SET_INTERRUPT_VECTOR(INTERRUPT_WIRELESS_RX, &isr_wireless_rx);
        SET_INTERRUPT_PRIORITY(INTERRUPT_WIRELESS_RX, 19);
        while (X32_wireless_char) c = X32_wireless_data; // empty buffer
        ENABLE_INTERRUPT(INTERRUPT_WIRELESS_RX);

	// prepare overflow/connection_problem interrupt handler	
        INTERRUPT_VECTOR(INTERRUPT_OVERFLOW) = &overflow_isr;
        INTERRUPT_PRIORITY(INTERRUPT_OVERFLOW) = 10;
	
	// start the test loop
    	ENABLE_INTERRUPT(INTERRUPT_GLOBAL); 
	
	control_qr(input,ae);
	X32_display = 0;
	
	while (! demo_done) {

		// Receiving frame loop	    
		for (v=0; v<10; v++) {
		// Checking whether there is a byte waiting to be read                               
		    do {
			ready_input = X32_rs232_char | 0x02;
		    } while (ready_input == 0 );
		    
			while ((c = getchar()) == -1){};              
				
			if ((i != 9) && (c != FRAMESTART) && (c!=ESCSTART)){
				new_values[i] = c;
				crc_values[i] = (unsigned char) new_values[i];			
				i++; 
			}
			if (c == FRAMESTART) {
				//previous= X32_ms_clock;
				i = 0;	
			}
			if(c == ESCSTART ){
				input->mode = PANICMODE;	
				control_qr(input, ae);	
				break;	
			}
			// If we complete the receiving without problem (start of new frame within the receiving of values)
			if (i == 9) {
		
				ENABLE_INTERRUPT(INTERRUPT_OVERFLOW);
			
				// Performing CRC check for the received values from PC -> FPGA
				if(crc (crc_values, 0)){
					input->mode = new_values[0];
					input->lift = new_values[1];
					input->roll = new_values[2];
					input->pitch = new_values[3];
					input->yaw = new_values[4];
					input->p = new_values[5];
					input->p1 = new_values[6];
					input->p2 = new_values[7];
					truemode = control_qr(input,ae);

			       		// Data logging values collection    
					if (time_divider == 14){
						time_divider = 0;
						es_ram[j++] = (unsigned int) ((X32_ms_clock & 0xff000000) >> 24);
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = (unsigned int) ((X32_ms_clock & 0x00ff0000) >> 16);
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = (unsigned int) ((X32_ms_clock & 0x0000ff00) >>  8);
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = (unsigned int) ((X32_ms_clock & 0x000000ff)      );
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = truemode;
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = (input->lift);
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = 0;	//-((p_b>>12));
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = 0;	//(unsigned int) (((ae[12]>>14) & 0x0000ff00) >>  8);
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = 0;	//(unsigned int) (((ae[12]>>14) & 0x000000ff));
						if (j ==  DL_CAPACITY)
							j = 0;
						es_ram[j++] = 5;
						if (j ==  DL_CAPACITY)
							j = 0;
					}
					else
						time_divider++;
					//*------------------------------------*

					do {
					    ready_output = X32_rs232_char | 0x01;
					} while (ready_output == 0 );

					// Rescaling rotor values in order to fit to unsigned chars for the communication
					ae0 = (ae_sp[0]>>3);
					ae1 = (ae_sp[1]>>3);
					ae2 = (ae_sp[2]>>3);
					ae3 = (ae_sp[3]>>3);
					putchar(frameflag);
					putchar(truemode);
					putchar(ae0);
					putchar(ae1);
					putchar(ae2);
					putchar(ae3);
					putchar(((input->p)+SCALE_FACTOR));
		 			putchar(((input->p1)+SCALE_FACTOR));
		 			putchar(((input->p2)+SCALE_FACTOR));
					crc_values[0] = truemode;
					crc_values[1] = ae0; 
					crc_values[2] = ae1;
					crc_values[3] = ae2; 
					crc_values[4] = ae3; 
					crc_values[5] = (((input->p)+SCALE_FACTOR));
					crc_values[6] = (((input->p1)+SCALE_FACTOR));
					crc_values[7] = (((input->p2)+SCALE_FACTOR)); 
					crc_values[8] = 0;
					// Computing the CRC remainder for the FPGA -> PC communication
					rem2pc = crc (crc_values, 1);
					putchar(rem2pc);
				}
			}
		}

		// Checking for overflow
		if (overflow == 1){
			input->mode = PANICMODE;	
			control_qr(input, ae);	
			break;	
		}	
		else if (c == ESCSTART)
			break;
		delay_ms(20);
	}
 	i = 0; 

    	// Data logging values sending
    	while (i < DL_CAPACITY){
		putchar(ESCSTART);
		putchar(es_ram[i]);
		putchar(es_ram[i+1]);
		putchar(es_ram[i+2]);
		putchar(es_ram[i+3]);
		putchar(es_ram[i+4]);  
		putchar(0);
		putchar(0);
		putchar(0);       
		crc_values2[0] = (unsigned char) es_ram[i];
		crc_values2[1] = (unsigned char) es_ram[i+1];
		crc_values2[2] = (unsigned char) es_ram[i+2];
		crc_values2[3] = (unsigned char) es_ram[i+3];
		crc_values2[4] = (unsigned char) es_ram[i+4];
		crc_values2[5] = 0;
		crc_values2[6] = 0;
		crc_values2[7] = 0; 
		crc_values2[8] = 0;
		rem2pc =  crc (crc_values2, 1);
		putchar(rem2pc);
		i = i+5;        
	}
	toggle_led(4);

    	DISABLE_INTERRUPT(INTERRUPT_GLOBAL);

	return 0;
}

