#ifndef COMMON_H_
#define COMMON_H_
#define FRAMESTART	255
#define ESCSTART	254
#define ENDSTART	253
#define DL_CAPACITY	600
#define SCALE_FACTOR	102

char malloc_memory[1024];
malloc_memory_size = 1024;

typedef struct vals{
 	int mode;
 	int lift;
 	int roll;
 	int pitch;
 	int yaw;
 	int p;
 	int p1;
 	int p2;
}Flightvals;



int ae_total;
int frameflag = FRAMESTART;
int	ae[15]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//int DELTA_AE=16;
//int test[]={0,0,0,0};
//int ae_sp[]={0,0,0,0};	//setpoints
int mode_isr;

#endif /*COMMON_H_*/
