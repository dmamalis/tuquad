/*  
 *  ISR
 *
 *	Engines mapping & filtering - Arnold de Jager
 *	isr_qr_link formulation - Kimon Tsitsikas
 *	
 *			    
 *--------------------------------------------------------------------------
 */
#ifndef MAIN_H_
#define MAIN_H_
#include <x32.h>
#include "common.h"

// define some peripheral short hands
#define X32_instruction_counter		peripherals[0x03]

#define X32_timer_per   	peripherals[PERIPHERAL_TIMER1_PERIOD]
#define X32_leds		peripherals[PERIPHERAL_LEDS]
#define X32_ms_clock		peripherals[PERIPHERAL_MS_CLOCK]
#define X32_us_clock		peripherals[PERIPHERAL_US_CLOCK]
#define X32_QR_a0 		peripherals[PERIPHERAL_XUFO_A0]
#define X32_QR_a1 		peripherals[PERIPHERAL_XUFO_A1]
#define X32_QR_a2 		peripherals[PERIPHERAL_XUFO_A2]
#define X32_QR_a3 		peripherals[PERIPHERAL_XUFO_A3]
#define X32_QR_s0 		peripherals[PERIPHERAL_XUFO_S0]
#define X32_QR_s1 		peripherals[PERIPHERAL_XUFO_S1]
#define X32_QR_s2 		peripherals[PERIPHERAL_XUFO_S2]
#define X32_QR_s3 		peripherals[PERIPHERAL_XUFO_S3]
#define X32_QR_s4 		peripherals[PERIPHERAL_XUFO_S4]
#define X32_QR_s5 		peripherals[PERIPHERAL_XUFO_S5]
#define X32_QR_timestamp 	peripherals[PERIPHERAL_XUFO_TIMESTAMP]

#define X32_rs232_data		peripherals[PERIPHERAL_PRIMARY_DATA]
#define X32_rs232_stat		peripherals[PERIPHERAL_PRIMARY_STATUS]
#define X32_rs232_char		(X32_rs232_stat & 0x02)

#define X32_wireless_data	peripherals[PERIPHERAL_WIRELESS_DATA]
#define X32_wireless_stat	peripherals[PERIPHERAL_WIRELESS_STATUS]
#define X32_wireless_char	(X32_wireless_stat & 0x02)

#define X32_button		peripherals[PERIPHERAL_BUTTONS]
#define X32_switches		peripherals[PERIPHERAL_SWITCHES]

#define X32_display		peripherals[0x05]

// 1st ORDER BUTTERWORTH FILTER COEFFICIENTS (Fs = 1267 Hz, Fc = 20)
#define a0 775 		// a coefficients, 14 bit fraction [^14]
#define a1 775
#define b0 1024		// b coefficients, 10 bit fraction [^10]
#define b1 927

// RX FIFO
#define FIFOSIZE 16
unsigned char	fifo[FIFOSIZE]; 
int	iptr, optr;

// Globals
unsigned char c;
int demo_done;

int new_values_flag = 0;
int new_values[9];
unsigned char crc_values[9];
unsigned char crc_values2[9];
unsigned char crc_values3[6];
int 	timestamp;		
int	isr_qr_counter;
int	isr_qr_time;
int	button;
int	inst;
int overflow = 0;
void toggle_led(int);
void delay_ms(int);
void delay_us(int);
int test[]={0,0,0,0};
int ae_sp[]={0,0,0,0};
int flag_first_filter = 1; 

int previous,now = 0;

// CALIBRATED SENSOR OUTPUTS
int zx,zy,zp,zq,zr; 		// Calibrated samples

// FILTERED SENSOR OUTPUTS
int fthe,fphi,fp,fq,fr; 	// Filtered samples

// 1st ORDER BUTTERWORTH FILTER
int x0 = 0;		// Filter variables
int x1 = 0;
int y1 = 0;
int y0 = 0;

// KALMAN FILTER
#define P2PHI 204 	// Rate to angle conversion factors, 10 bit fraction [^10]
#define Q2THE 192 		

int p_b = 0;		// Roll variables
int phi = 0;
int p = 0;

int q_b = 0;		// Pitch variables
int the = 0;
int q = 0;

// CONTROLLER SET POINTS
int L_s = 0;		// Roll
int M_s = 0;		// Pitch
int N_s = 0;		// Yaw

enum QRmodes {SAFEMODE , PANICMODE, MANUALMODE, CALIBRATIONMODE, YAWMODE, FULLMODE};
enum QRmodes qrmode;

/*------------------------------------------------------------------
 * isr_qr_link -- QR link rx interrupt handler
 *------------------------------------------------------------------
 */
void isr_button(void)
{
	button = 1;
}

/*------------------------------------------------------------------
 * isr_qr_link -- QR link rx interrupt handler
 *------------------------------------------------------------------
 */
void isr_qr_link(void)
{
	
	int	ae_index;
	ae_total = X32_QR_a0 + X32_QR_a1 + X32_QR_a2 + X32_QR_a3;
	
	// record time
	isr_qr_time = X32_ms_clock;
        inst = X32_instruction_counter;
		
	// Get timestamp value
	timestamp = X32_QR_timestamp;

	// monitor presence of interrupts 
	isr_qr_counter++;
	if (isr_qr_counter % 500 == 0) {
		toggle_led(7);
	}
		
	if (ae[7] == 5) {	// FULL CONTROL MODE

		// Calibrating the samples
		zx = ( (X32_QR_s0 << 14) - ae[8] );	// 14 bit fraction [^14]
		zy = ( (X32_QR_s1 << 14) - ae[9] );
		zp = ( (X32_QR_s3 << 14) - ae[10]);
		zq = ( (X32_QR_s4 << 14) - ae[11]);
		zr = ( (X32_QR_s5 << 14) - ae[12]);		

		// -------------------------------------------------------------//
		// 1st order Butterworth filter (Fs = 1267 Hz, Fc = 20)		//
		//--------------------------------------------------------------//

		x0 = (zr >> 4);		// x's use 10 bit fraction [^10]

		// Filter initialization - x1 and y1 are assigned initial values
		if(flag_first_filter) {		
			x1 = x0;		// x's use 10 bit fraction [^10]
			y1 = (x0 << 4);		// y's use 14 bit fraction [^14]
			flag_first_filter = 0;
		}

		// Multiplication implementation
		y0 = (a0*x0) + (a1*x1) + (b1*y1);	// z0 [^24] = a0*x0 [^14*^10] + a1*x1 [^14*^10] + b1*y1 [^10*^14]
		y0 = (y0 >> 10);			// Shift from [^24] to [^14] standard for y values

		// Shifting implementation
		x1 = x0; y1 = y0;

		// -------------------------------------------------------------//
		// Kalman filter (C1 = 2^9 Hz, C2 = 2^18)			//
		//--------------------------------------------------------------//

		// Kalman filtering p, phi
		fp = zp - ae[13];					 //  [^14] - [^14]
		fphi = (fphi + ((fp*P2PHI) >> 10) );			 //  [^14] + [^14]*[^10] >> 10
		fphi = (fphi - ((fphi - zy) >> 9) );			 //  [^14] - ([^14]-[^14])/2^9 
		ae[13] = (( (ae[13] << 6) + ((fphi - zy) >> 12) ) >> 6); // ([^20] + (([^14]-[^14]) << 6)/2^18 ) >> 6

		//Kalman filtering q, theta
		fq = zq - ae[14];					 //  [^14] - [^14]
		fthe = (fthe + ((fq*Q2THE) >> 10 ) );			 //  [^14] + [^14]*[^10] >> 10
		fthe = (fthe - ((fthe - zx) >> 9 ) );			 //  [^14] - ([^14]-[^14])/2^9
		ae[14] = (( (ae[14] << 6) + ((fthe - zx) >> 12) ) >> 6); // ([^20] + (([^14]-[^14]) << 6)/2^18 ) >> 6

		// -------------------------------------------------------------//
		// Cascaded P controller					//
		// -------------------------------------------------------------//

		// Angle control ( ae[5] = P1, ae[6] = P2 )
		L_s = -(( ae[6]* ((-ae[1]) - fphi) - ae[5]*fp ) >> 17);	 // 17 bit shift for scaling, P1 & P2 [^3]
		M_s = -(( ae[6]* ((-ae[2]) - fthe) - ae[5]*fq ) >> 17);	
		
		// Rate control ( ae[4] = P )
		N_s = -(( ae[4]* ((-ae[3]) - y0) ) >> 14);

		// -------------------------------------------------------------//		
		
	}
	else if (ae[7] == 4) {		// RATE CONTROL MODE

		// Calibrating the samples
		zr = ( (X32_QR_s5 << 14) - ae[12] );	// 14 bit fraction [^14]

		// -------------------------------------------------------------//
		// 1st order Butterworth filter (Fs = 1267 Hz, Fc = 20)		//
		//--------------------------------------------------------------//

		x0 = (zr >> 4);		// x's use 10 bit fraction [^10]

		// Filter initialization - x1 and y1 are assigned initial values
		if(flag_first_filter) {		
			x1 = x0;		// x's use 10 bit fraction [^10]
			y1 = (x0 << 4);		// y's use 14 bit fraction [^14]
			flag_first_filter = 0;
		}

		// Multiplication implementation
		y0 = (a0*x0) + (a1*x1) + (b1*y1);	// [^24] = [^14*^10] + [^14*^10] + [^10*^14]
		y0 = (y0 >> 10);			// Shift from [^24] to [^14] standard for y values

		// Shifting implementation
		x1 = x0; y1 = y0;

		// -------------------------------------------------------------//
		// Yaw rate P controller					//
		// -------------------------------------------------------------//

		// Rate control ( ae[4] = P )
		N_s = -((ae[4] * ((-ae[3]) - y0))>>14);

		// Assign roll/pitch outputs
		M_s = ae[2];
		L_s = ae[1];

		// -------------------------------------------------------------// 

	}
	else {		// NO CONTROL MODES

		// Assign outputs
		N_s = ae[3];
		M_s = ae[2];
		L_s = ae[1];

	}

	// Assign current engine set-points to test variables for slew limiter
	test[0] = ae_sp[0];	
	test[1] = ae_sp[1];
	test[2] = ae_sp[2];
	test[3] = ae_sp[3];

	// Determine engine set-points
	ae_sp[0] = (ae[0] + (M_s<<1) - N_s); 	
	ae_sp[1] = (ae[0] - (L_s<<1) + N_s); 
	ae_sp[2] = (ae[0] - (M_s<<1) - N_s); 
	ae_sp[3] = (ae[0] + (L_s<<1) + N_s);

	// Clip engine outputs and limit to positive and max 1016
	for (ae_index = 0; ae_index < 4; ae_index++) {
							
		// Positive and limited engine output		
		if (ae_sp[ae_index] < 0)
			ae_sp[ae_index] = 0;
		else if (ae_sp[ae_index] > 1016)
			ae_sp[ae_index] = 1016;
	
	}

	// Send actuator values
	X32_QR_a0 = ae_sp[0];	
	X32_QR_a1 = ae_sp[1];
	X32_QR_a2 = ae_sp[2];
	X32_QR_a3 = ae_sp[3];

	// record isr execution time (ignore overflow)
        inst = X32_instruction_counter - inst;
	isr_qr_time = X32_ms_clock - isr_qr_time;
}

/*------------------------------------------------------------------
 * isr_rs232_rx -- rs232 rx interrupt handler
 *------------------------------------------------------------------
 */
void isr_rs232_rx(void)
{
	int	c;

	// signal interrupt
	toggle_led(6);

	// may have received > 1 char before IRQ is serviced so loop
	while (X32_rs232_char) {
		fifo[iptr++] = X32_rs232_data;
		if (iptr > FIFOSIZE)
			iptr = 0;
	}

}

/*------------------------------------------------------------------
 * getchar -- read char from rx fifo, return -1 if no char available
 *------------------------------------------------------------------
 */
int 	getchar(void)
{
	int	c;

	if (optr == iptr)
		return -1;
	c = fifo[optr++];
	if (optr > FIFOSIZE)
		optr = 0;
	return c;
}


/*------------------------------------------------------------------
 * isr_wireless_rx -- wireless rx interrupt handler
 *------------------------------------------------------------------
 */
void isr_wireless_rx(void)
{
	int c;

	// signal interrupt
	toggle_led(7);


	// may have received > 1 char before IRQ is serviced so loop
	while (X32_wireless_char) {
		fifo[iptr++] = X32_wireless_data;
		if (iptr > FIFOSIZE)
			iptr = 0;
	}

}

/*------------------------------------------------------------------
 * delay_ms -- busy-wait for ms milliseconds
 *------------------------------------------------------------------
 */
void delay_ms(int ms) 
{
	int time = X32_ms_clock;
	while(X32_ms_clock - time < ms)
		;
}

/*------------------------------------------------------------------
 * delay_us -- busy-wait for us microseconds
 *------------------------------------------------------------------
 */
void delay_us(int us) 
{
	int time = X32_us_clock;
	while(X32_us_clock - time < us)
		;
}

/*------------------------------------------------------------------
 * toggle_led -- toggle led # i
 *------------------------------------------------------------------
 */
void toggle_led(int i) 
{
	X32_leds = (X32_leds ^ (1 << i));
}


/*------------------------------------------------------------------
 * print_state -- print all sensors and actuators
 *------------------------------------------------------------------
 */

void print_state(void) 
{
	int i;
	char text[100] , a;
	printf("%d %d %d %d %d\n", new_values[0],new_values[1],new_values[2],new_values[3],new_values[4]);
    	i = 0;
    	while( text[i] != 0) {
       		delay_ms(1);
		// if (X32_switches == 0x03)
		if (X32_wireless_stat & 0x01 == 0x01)
			X32_wireless_data = text[i];

		i++;
    	}
}

/*------------------------------------------------------------------
 * overflow_isr -- overflow/connection_problem interrupt handler
 *------------------------------------------------------------------
 */
void overflow_isr() {
        toggle_led(5);
        overflow = 1;
}

/* filter_2ndBLP 
 * Description - 2nd order BW filter 10Hz with fixed point scheme
 * 				 At initial filtering attempts 2nd order BW where selected as the proper filter.
 * 				 To that extend two distinct filter codes where produced and ultimately the optimal version was promoted for importing
 *				 This alternate approach inlcudes a fixed point arithmetic scheme of 11bits but was rejected as slower and not as
 * 				 accurate as the final versions after conducting measuring experiments.
 * @param      - int z_i 	the calibrated value to be filtered
 * @return     - int 		the filtered value
 * @author     - Nikos Larisis	

 */
 /*int filter_2ndBLP(int z_i){
	
	x0 = z_i<<8;
	
	if(flag_first_filter){
		x1 = x0;
		x2 = x0;
		Y1 = x0;
		Y2 = x0;
		flag_first_filter = 0;
	}

	y0 = (a0*x0) + (a0*2*x1) + (a0*x2) + (b1*y1) - (b2*y2);
	
	y0 = y0 >> 11;
	
	x2 = x1; x1 = x0; Y2 = Y1; Y1 = Y0;		
	
	return y0>>8;
 
 }*/

#endif /*MAIN_H_*/
