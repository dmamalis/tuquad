/*  
 *  Quad Rotor Controller
 *
 *	Kimon Tsitsikas & Arnold de Jager
 *	
 *	Mods: @Calibration mode by Nikos Larisis
 *			    FSM by Dimitris Mamalis
 *--------------------------------------------------------------------------
 */

/************************************************************************
*	IMPORTANT NOTICE						*
*	----------------						*
*	In order to make these values accessible to the			*
*	interrupt handler we extended and remapped the 			*
*	ae[] array. We use the ae_sp[] array as the one			*
*	which includes the rotor values.				*
*************************************************************************/
/*	ae[0] : lift
	ae[1] : roll
	ae[2] : pitch
	ae[3] : yaw
	ae[4] : p
	ae[5] : p1
	ae[6] : p2
	ae[7] : mode
	ae[8] : sax0;
	ae[9] : say0;
	ae[10] : sp0;
	ae[11] : sq0;
	ae[12] : sr0;
	ae[13] : p_b;
	ae[14] : q_b;
	ae_sp[0] : rotor1
	ae_sp[1] : rotor2
	ae_sp[2] : rotor3
	ae_sp[3] : rotor4
 */

#include <stdio.h>
#include "./lib/control_qr.h"
#include "./lib/common.h"
#include "./lib/main.h"

int currmode = 0;

// CALIBRATION MODE variables
int flag_finished = 0;		// indicate whether CALIBRATIONMODE has finished calculations
int flag_first = 0;		// initialize the vectors only at the first run of CALIBRATIONMODE

#define N 512 			// number of samples for calibration (2^9)
#define M 5			// additional shift to average calibration sum to 14 bit fraction: M = 14 - (power of N) [^9]

int sax_v[N],sum_sax_v=0;	// initialize calibration sums
int say_v[N],sum_say_v=0;
int sp_v[N],sum_sp_v=0;
int sq_v[N],sum_sq_v=0;
int sr_v[N],sum_sr_v=0;
int sum_pb, sum_qb = 0;
int position = 0;

int sax0,say0,sp0,sq0,sr0; 	// DC offset of samples		

int calib_ready = 0;


/*--------------------------------------------------------------------------
 *  QR Control Loop
 *--------------------------------------------------------------------------
 */

int	control_qr(Flightvals *input, int ae[])
{
	// Create local variables
	int j = 0;
	int i = 0;

	

//#########################################################

	// Reverse Scaling
			
	input->roll = 	(input->roll) 	- SCALE_FACTOR;
	input->pitch = 	(input->pitch) 	- SCALE_FACTOR;
	input->yaw = 	(input->yaw) 	- SCALE_FACTOR;
	input->p = 	(input->p) 	- SCALE_FACTOR;
	input->p1 = 	(input->p1) 	- SCALE_FACTOR;
	input->p2 = 	(input->p2) 	- SCALE_FACTOR;

//#########################################################	

	if (currmode == input->mode){	
	}
	else if(currmode == SAFEMODE && input->mode != MANUALMODE){
	}
	else if(currmode == SAFEMODE && (input->lift != 0 || input->roll != 0 || input->pitch != 0 || input->yaw != 0)){
	}
	else if(calib_ready!=1 && input->mode > CALIBRATIONMODE){
	}
	else if(currmode > PANICMODE && input->mode > PANICMODE && (ae[0] != 0 || ae[1] != 0 || ae[2] != 0 || ae[3] != 0)){
	}
	else if(currmode == PANICMODE && input->mode >= MANUALMODE){
	}
	else{
		currmode = input->mode;
	}
	


	if (overflow == 1){
		input->mode = PANICMODE;
		currmode = PANICMODE;
	}
	
	// Control QR - MODES
	switch ( currmode ){			
		
		case SAFEMODE:	

			ae[7] = 0;			// Set current mode (ae[7]) to 0 (SAFEMODE)

			ae[0] = 0;			// Assign control inputs
			ae[1] = 0; 
			ae[2] = 0; 
			ae[3] = 0;
			
			ae[4] = (input->p);		// Assign proportional control parameters
			ae[5] = (input->p1);
			ae[6] = ((input->p2)<<1);	// Scale p2
			
			toggle_led(0);
			break;		

		case PANICMODE:	
							
			/* If qr is hovering first go and stay for an amount of */
			/* time to panic mode (lift = 35) and then to safe mode */
			if (ae_sp[0] >= 470 || ae_sp[1] >= 470 || ae_sp[2] >= 470 || ae_sp[3] >= 470)
			{
				
				for(j=0;j<50000;j++)
				{
					ae[7] = 1;	// Set current mode (ae[7]) to 1 (PANICMODE)
					ae[0] = 450;	
					ae[1] = 0; 
					ae[2] = 0; 
					ae[3] = 0;
					ae[4] = 0;
					ae[5] = 0;
					ae[6] = 0;

					toggle_led(1);
				}
			}
			input->mode = SAFEMODE;
			currmode = SAFEMODE;
			for(j=0;j<50000;j++)
			{
				ae[7] = 0;		// Set current mode (ae[7]) to 0 (SAFEMODE)
				ae[0] = 0;		

				toggle_led(0);
				toggle_led(3);
			}
			break;		

		case MANUALMODE:

			ae[7] = 2;	// Set current mode ae[7] to 2 (MANUALMODE)

			// Lift input 
			// - 13 joystick inputs scaled from 0 to 65 (increment by 5)
			// - trim inputs increment by 1

			// Scale lift (ae[0])		     		   | range      | increment
			if ((input->lift <= 50) && (input->lift > 25))	 	
				ae[0] = (input->lift)*2 + 480;	// 	   | 532 to 580 |    2
			else if (input->lift <= 25)
				ae[0] = (input->lift)*21;	//	   |   0 to 525 |   21
			else if (input->lift > 50)
				ae[0] = (input->lift)*20 - 430;	// 	   | 590 to 870 |   20
			
			// Assign control inputs
			ae[1] = (input->roll); 		
			ae[2] = (input->pitch); 
			ae[3] = ((input->yaw)<<2);

			// Assign proportional control parameters
			ae[4] = (input->p);		
			ae[5] = (input->p1);
			ae[6] = ((input->p2)<<1);
			
			toggle_led(2);	
			break;
		
		case CALIBRATIONMODE:

			ae[7] = 3;	// Set current mode ae[7] to 3 (CALIBRATIONMODE)
			toggle_led(3);

	
			if (flag_finished == 0) {	// If calibration not finished			
		
				// Maintain sensor values for 30'' or as long as CALIBRATIONMODE is ON 	
				while(1){		

					sum_sax_v += X32_QR_s0;
					sum_say_v += X32_QR_s1;
					sum_sp_v  += X32_QR_s3;
					sum_sq_v  += X32_QR_s4;
					sum_sr_v  += X32_QR_s5;

					position++;
					if (position == N) {
						
						ae[8]  = (sum_sax_v << M);
						ae[9]  = (sum_say_v << M);
						ae[10] = (sum_sp_v << M);
						ae[11] = (sum_sq_v << M);
						ae[12] = (sum_sr_v << M);

						sum_sax_v = 0;
						sum_say_v = 0;
						sum_sp_v = 0;
						sum_sq_v = 0;
						sum_sr_v = 0;
						
						flag_finished = 1;	// if values are calculated dont do the loop again 
						break;
					}
				}
				
				//Bias Calibration
				for (i = 0; i < 4608; i++) {

					// Calibrating the samples
					zx = ( (X32_QR_s0 << 14) - ae[8] );	// 14 bit fraction [^14]
					zy = ( (X32_QR_s1 << 14) - ae[9] );
					zp = ( (X32_QR_s3 << 14) - ae[10]);
					zq = ( (X32_QR_s4 << 14) - ae[11]);
					zr = ( (X32_QR_s5 << 14) - ae[12]);

					// -------------------------------------------------------------//
					// Kalman filter (C1 = 2^10 Hz, C2 = 2^16)			//
					//--------------------------------------------------------------//

					// Kalman filtering p, phi
					fp = zp - p_b;				//  [^14] - [^14]
					fphi = (fphi + ((fp*P2PHI) >> 10) );	//  [^14] + [^14]*[^10] >> 10
					fphi = (fphi - ((fphi - zy) >> 10));	//  [^14] - ([^14]-[^14])/2^10 
					p_b = (( (p_b << 6) + ((fphi - zy) >> 10) ) >> 6); 
										// ([^20] + (([^14]-[^14]) << 6)/2^16 ) >> 6
					//Kalman filtering q, theta
					fq = zq - q_b;				//  [^14] - [^14]
					fthe = (fthe + ((fq*Q2THE) >> 10) );	//  [^14] + [^14]*[^10] >> 10
					fthe = (fthe - ((fthe - zx) >> 10));	//  [^14] - ([^14]-[^14])/2^10
					q_b = (( (q_b << 6) + ((fthe - zx) >> 10) ) >> 6); 
										// ([^20] + (([^14]-[^14]) << 6)/2^16 ) >> 6
					
					//--------------------------------------------------------------//

					// Delay in order to achieve the desired sampling frequency 
					for (j = 0; j < 85; j++) {}
					
					// Sum filter output after first 512 samples
					if (i>512) {
						sum_pb += p_b;
						sum_qb += q_b;
					}
				}

				// Average and assign bias values (4096 samples -> 2^12)
				ae[13] = (sum_pb >> 12);
				ae[14] = (sum_qb >> 12);

			}
			else {		// If calibration has been completed

				calib_ready = 1;
				break;

			}			
			break;							
			
		case YAWMODE:
		
			ae[7] = 4;	// Set current mode ae[7] to 4 (YAWMODE)

			// Lift input 
			// - 13 joystick inputs scaled from 0 to 65 (increment by 5)
			// - trim inputs increment by 1

			// Scale lift (ae[0])		     		   | range      | increment
			if ((input->lift <= 50) && (input->lift > 25))	 	
				ae[0] = (input->lift)*2 + 480;	// 	   | 532 to 580 |    2
			else if (input->lift <= 25)
				ae[0] = (input->lift)*21;	//	   |   0 to 525 |   21
			else if (input->lift > 50)
				ae[0] = (input->lift)*20 - 430;	// 	   | 590 to 870 |   20

			// Assign control inputs			
			ae[1] = (input->roll); 
			ae[2] = (input->pitch); 
			ae[3] = (input->yaw)*5000;
			
			// Assign proportional control parameter
			if (ae[0] == 0)
				ae[4] = 0;
			else
				ae[4] = ((input->p)<<3);

			toggle_led(4);			 
			break;

		case FULLMODE:

			ae[7] = 5;	// Set current mode ae[7] to 5 (FULLMODE)

			// Lift input 
			// - 13 joystick inputs scaled from 0 to 65 (increment by 5)
			// - trim inputs increment by 1

			// Scale lift (ae[0])		     		   | range      | increment
			if ((input->lift <= 50) && (input->lift > 25))	 	
				ae[0] = (input->lift)*2 + 480;	// 	   | 532 to 580 |    2
			else if (input->lift <= 25)
				ae[0] = (input->lift)*21;	//	   |   0 to 525 |   21
			else if (input->lift > 50)
				ae[0] = (input->lift)*20 - 430;	// 	   | 590 to 870 |   20
			
			// Assign control inputs			
			ae[1] = (input->roll)*10000; 		
			ae[2] = (input->pitch)*10000; 
			ae[3] = (input->yaw)*5000;
			
			// Assign proportional control parameters
			if (ae[0] == 0) {
				ae[4] = 0;
				ae[5] = 0;
				ae[6] = 0;
			}
			else {
				ae[4] = ((input->p)<<3);	
				ae[5] = ((input->p1)<<4);
				ae[6] = ((input->p2));		// p1,p2 scaled [^3] through additional controller bit shift
			}

			toggle_led(5);
			break;
		
		default:
			break;
	}

	return currmode;
}




