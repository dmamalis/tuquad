/*  
 *  PC side FSM & Keyboard-Joystick values multiplexing 
 *
 *	Dimitris Mamalis
 *	
 *
 *--------------------------------------------------------------------------
 */

#include "./lib/common.h"
#include "./lib/control_pc.h"
#define SCALE_FACTOR 102
int calib_ready = 0;

void control_pc(Flightvals *userstate,Flightvals *kb_state,Flightvals *js_state,int cor_values[]){
	
	userstate->mode = cor_values[0];

	// FSM - Checking whether changing mode is allowed or not	
	if (userstate->mode == kb_state->mode){	
	}
	else if(userstate->mode == SAFEMODE && kb_state->mode != MANUALMODE){
	}
	else if(userstate->mode == SAFEMODE && (cor_values[1] != 0 || cor_values[2] != 0 || cor_values[3] != 0 || cor_values[4] != 0)){
	}
	else if(calib_ready!=1 && kb_state->mode > CALIBRATIONMODE){
	}
	else if(userstate->mode > PANICMODE && kb_state->mode > PANICMODE && (cor_values[1] != 0 || cor_values[2] != 0 || cor_values[3] != 0 || cor_values[4] != 0)){
	}
	else if(userstate->mode == PANICMODE && kb_state->mode >= MANUALMODE){
	}
	else{
		userstate->mode = kb_state->mode;
	}
	
	
	switch(userstate->mode){
	
	case SAFEMODE:
	case PANICMODE:
	case CALIBRATIONMODE:
		kb_state->lift = 0;
		kb_state->roll = 0;
		kb_state->pitch = 0;
		kb_state->yaw = 0;
		kb_state->p =0;
		kb_state->p1=0;
		kb_state->p2=0;		
		js_state->lift = 0;
		js_state->roll = 0;
		js_state->pitch = 0;
		js_state->pitch = 0;
		js_state->yaw = 0;
		userstate->lift = kb_state->lift + js_state->lift;
		userstate->roll = kb_state->roll + js_state->roll 		+SCALE_FACTOR;
		userstate->pitch = kb_state->pitch + js_state->pitch	+SCALE_FACTOR;
		userstate->yaw = kb_state->yaw + js_state->yaw 			+SCALE_FACTOR;
		calib_ready = 1;
		break;
	case MANUALMODE:
		userstate->lift = (kb_state->lift) + 5*(js_state->lift);
		if (userstate->lift<0)
			userstate->lift = 0;
		userstate->roll = kb_state->roll + 6*(js_state->roll) 		+SCALE_FACTOR;
		userstate->pitch = kb_state->pitch + 6*(js_state->pitch)	+SCALE_FACTOR;
		userstate->yaw = (kb_state->yaw) + 6*(js_state->yaw) 			+SCALE_FACTOR;
		userstate->p =kb_state->p + SCALE_FACTOR;
		userstate->p1 =kb_state->p1 + SCALE_FACTOR;
		userstate->p2 =kb_state->p2 + SCALE_FACTOR;
		break;
	case YAWMODE:
	case FULLMODE:
		userstate->lift = (kb_state->lift) + 5*(js_state->lift);
		if (userstate->lift<0)
			userstate->lift = 0;
		userstate->roll = kb_state->roll + 6*(js_state->roll) 		+SCALE_FACTOR;
		userstate->pitch = kb_state->pitch + 6*(js_state->pitch)	+SCALE_FACTOR;
		userstate->yaw = (kb_state->yaw) + 6*(js_state->yaw) 			+SCALE_FACTOR;
		userstate->p =kb_state->p + SCALE_FACTOR;
		userstate->p1 =kb_state->p1 + SCALE_FACTOR;
		userstate->p2 =kb_state->p2 + SCALE_FACTOR;
		break;
	default:
		break;
	}
}
