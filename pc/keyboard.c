/*  
 *  Keyboard mapping
 *
 *	Dimitris Mamalis
 *	
 *
 *--------------------------------------------------------------------------
 */

#include "./lib/keyboard.h"
#include "./lib/common.h"

void keyboard(char c, Flightvals *kb_state,int cor_values[]){
	
	kb_state->mode = cor_values[0];
	switch(c){

		case 27:							//ESC


			if(term_getchar_nb() == 91){		//dummy
				switch(term_getchar_nb()){
					case 68:				//RWT
					/*
					 * 	PASS  & CHECK TRUEMODE
					 */
						if(kb_state->mode >PANICMODE){
							kb_state->roll = (kb_state->roll) +1;
						}
						break;
					case 67:
						if(kb_state->mode >PANICMODE){
							kb_state->roll = (kb_state->roll) -1;
						}
						break;
					case 66:				//DWN
						if(kb_state->mode >PANICMODE){
							kb_state->pitch = (kb_state->pitch) +1;
						}		
						break;
					case 65:				//UP
						if(kb_state->mode >PANICMODE){
							kb_state->pitch = (kb_state->pitch) -1;
						}
						break;					
					default:
						break;
				}
			}
			else
				//kb_state->mode = 1;
				frameflag = ESCSTART;
			break;
		//case 13:							//Serial changing of modes mapped to space bar
		case 32:							//SPACE
			kb_state->mode = (kb_state->mode + 1) % 6;
			break;
		
		case '0':							//SAFE MODE
			kb_state->mode = SAFEMODE;			
			break;						
		case '1':							//PANIC MODE
			kb_state->mode = PANICMODE;
			break;
		case '2':							//MANUAL MODE
			kb_state->mode = MANUALMODE;
			break;
		case '3':							//CALIBRATION MODE
			kb_state->mode = CALIBRATIONMODE;	
			break;
		case '4':							//YAW MODE
			kb_state->mode = YAWMODE;	
			break;
		case '5':							//FULL MODE
			kb_state->mode = FULLMODE;
			break;		
		case 'a':							//Lift
			if(kb_state->mode >PANICMODE){
				kb_state->lift = (kb_state->lift) +1;
			}
			break;
		case 'z':
			if(kb_state->mode >PANICMODE){
				kb_state->lift = (kb_state->lift) -1;
			}
			break;	
		case 'q':							//Yaw
			if(kb_state->mode >PANICMODE){
				kb_state->yaw = (kb_state->yaw) +1;
			}
			break;
		case 'w':			
			if(kb_state->mode >PANICMODE){
				kb_state->yaw = (kb_state->yaw) -1;
			}
			break;
		case 'u':							//Yaw control P up/down
			if(kb_state->mode == YAWMODE || kb_state->mode == FULLMODE){
				kb_state->p++;
			}
			break;
		case 'j':
			if(kb_state->mode == YAWMODE || kb_state->mode == FULLMODE){
				kb_state->p--;
			}
			break;
		case 'i':							//Roll/Pitch control P1 up/down
			if(kb_state->mode == FULLMODE){
				kb_state->p1++;
			}
			break;
		case 'k':
			if(kb_state->mode == FULLMODE){
				kb_state->p1--;
			}
			break;
		case 'o':							//Roll/Pitch control P2 up/down
			if(kb_state->mode == FULLMODE){
				kb_state->p2++;
			}
			break;
		case 'l':
			if(kb_state->mode == FULLMODE){
				(kb_state->p2)--;
			}
			break;

		default:
			break;
			
	}
}
