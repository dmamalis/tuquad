/* crc.c
 * Description - performing crc of the exchanged frame containing the appropriate values (mode, lift, roll, pitch, yaw, etc).
 *			     In our implementation we use 9-bit CRC scheme and selected the following divisor,
 *			     x^8+x^7+x^4+x^3+x+1 => binary interpretation 1 1001 1011
 *			     By this way we achieve a maximum security in the communication for the 8-bit channel (since chars are the transfered
 *			     vehicle). The function is parameterized in such way so that the same block code will be used in both sides, PC and QR.
 *			     The function can be further parameterized since #define declarations dictate the final value of the length of the frame
 * @param  	   - the five values sent from PC to fpga
 *	         	 flag = 1 - COMPUTE the CRC remainder
 *	         	 flag = 0 - CHECK the CRC remainder 
 * @author 	   - Nikos Larisis	
 */
#define N 9 			// the length of the frame in terms of contents, 9 values will be transfered
#define M (N<<3)		// the length of the frame in terms of bits, 9*8

int crc (unsigned char * in, int flag){

	unsigned char fr[N]; 									// frame contains mode, lift, roll, pitch, yaw, P, P1, P2, and the remainder
	unsigned char frame[M];
	
	int i,j,k,kk,dd,firstbit,first,result=0,crc_result=0;
	char divisor[9] = {1,1,0,0,1,1,0,1,1};	

	for(i=0; i<N; i++){
		fr[i] = in[i];
	}
	
	for (i=M-1,j=1,k=N-1,kk=8; i>-1; i--){
		frame[i] = fr[k] & j; 
		if(frame[i]){frame[i]=1;} else {frame[i]=0;}
		j = j << 1;
		for (dd=0; dd<N; dd++){
			if (i==M-kk){k--;j=1;kk+=8;}
		}
	}
	
	
	// ----------  Conducting the CRC process   ---------------
	
	for(j=0; j<M-8; j++){			// locating the start of the bit sequence  
		if(frame[j]==1){
			firstbit = j;
			break;
		}		
	}
	
	// CHECK the CRC remainder 
	if(!flag) {	
		for (first=firstbit; first<M-17; first++){
			
			while(frame[first]==0){if(first>M-17){break;}first++;}	// locating the  NEW start of the bit sequence, because	after 
										// each iteration the "new" frame is shifted
			for(i=0,j=first; i<9; i++,j++){  			// 9 is the length of divisor
				frame[j] = frame[j] ^ divisor[i];	// each time the contents of the frame are XORED with the divisor
			}							// this process stops at the designated point
		}
		
		for (i=M-9,j=0;i>M-17;i--,j++){				// then, the CRC remainder equals last 8 XORED bits of the frame 
			if(frame[i]){crc_result += ((int)frame[i] << j);}			
		}	
			
		if((unsigned char)crc_result == fr[8]){				// new CRC is compared with the "received" CRC stored in fr[8]
			return 1;
		}
		return 0;
	}
	
	// COMPUTE the CRC remainder
	else {
		for (first=firstbit; first<M-17; first++){
			
			while(frame[first]==0){if(first>M-17){break;}first++;}		// locating the  NEW start of the bit sequence 
			
			for(i=0,j=first; i<9; i++,j++){
				frame[j] = frame[j] ^ divisor[i];
			}
		}
		
		for (i=M-9,j=0;i>M-17;i--,j++){
			if(frame[i]){result += ((int)frame[i] << j);}			
		}		
		return result;
	}	
}
