/*  
 *  GUI - State printing
 *
 *	Nikos Larisis
 *	
 *
 *--------------------------------------------------------------------------
 */

#include "./lib/common.h"
#include "./lib/print_state.h"

int flag_calibr_ready = 0;
int previous_mode = 6;

void print_state(Flightvals *userstate){

	if ((userstate->mode) != previous_mode)
	{
		switch(userstate->mode){
	
		case SAFEMODE:		
			if(flag_calibr_ready){
				term_puts("\n\n|-------------------------------------------------|");
				term_puts("\n|\t\tS A F E   M O D E \t\t  |"); 
				term_puts("\n|-------------------------------------------------|\n");
				term_puts("\nPress - 2 to enter Manual Mode \n");
				term_puts("      - 4 to enter Yaw Control Mode\n");
				term_puts("      - 5 to enter Full Control Mode\n");
				term_puts("      - ESC to exit...\n");
				term_puts("\n|-------------------------------------------------|\n\n");
			}
			else{
				term_puts("\n\n|-------------------------------------------------|");
				term_puts("\n|\t\tS A F E   M O D E \t\t  |"); 
				term_puts("\n|-------------------------------------------------|\n");
				term_puts("\nPress - 2 to enter Manual Mode \n      - ESC to exit...");
				term_puts("\n|-------------------------------------------------|\n\n");
			}
			break;	
		case PANICMODE:
			term_puts("\n\n|-------------------------------------------------|");
			term_puts("\n|\t\tP A N I C   M O D E \t\t  |"); 
			term_puts("\n|-------------------------------------------------|\n");
			term_puts("\nSystem exiting...");
			term_puts("\n|-------------------------------------------------|\n\n");	
			break;
		case CALIBRATIONMODE:
			term_puts("\n\n|-------------------------------------------------|");
			term_puts("\n|\tC A L I B R A T I O N   M O D E \t  |"); 
			term_puts("\n|-------------------------------------------------|\n");
			term_puts("\nCollecting non-zero DC offset samples...");
			term_puts("\n|-------------------------------------------------|\n\n");
			flag_calibr_ready = 1;		
			break;
		case MANUALMODE:
			term_puts("\n\n|-------------------------------------------------|");
			term_puts("\n|\t\tM A N U A L   M O D E \t\t  |"); 
			term_puts("\n|-------------------------------------------------|\n");
		
			term_puts("\nPilot control keys:\n");
			term_puts("[kb]a      or  [js]throttle-<up>  : increase lift\n");
			term_puts("[kb]z      or  [js]throttle-<dwn> : decrease lift\n");
			term_puts("[kb]w      or  [js]twist-clockwise: increase yaw\n");
			term_puts("[kb]q      or  [js]twist-countercl: decrease yaw\n");
			term_puts("[kb]<up>   or  [js]forward        : increase pitch\n");
			term_puts("[kb]<dwn>  or  [js]backward       : decrease pitch\n");
			term_puts("[kb]<lft>  or  [js]<lft>          : increase roll\n");
			term_puts("[kb]<rt>   or  [js]<rt>           : decrease roll\n");
			term_puts("[kb]<ESC>  or  [js]<FIRE>         : abort/exit\n");
			term_puts("[kb]0 up to 5                     : select mode\n");
			term_puts("[kb]<ENTER>                       : jump next mode\n");
			term_puts("\n|-------------------------------------------------|\n\n");	
			break;
		case YAWMODE:
			term_puts("\n\n|-------------------------------------------------|");
			term_puts("\n|\t\tY A W   M O D E \t\t  |"); 
			term_puts("\n|-------------------------------------------------|\n");
		
			term_puts("\nEXTRA Pilot control keys:\n");
			term_puts("[kb]u                       : increase P parameter\n");
			term_puts("[kb]j                       : decrease P parameter\n");
			term_puts("\n|-------------------------------------------------|\n\n");
			break;
		case FULLMODE:
			term_puts("\n\n|-------------------------------------------------|");
			term_puts("\n|\tF U L L   C O N T R O L   M O D E \t  |"); 
			term_puts("\n|-------------------------------------------------|\n");
		
			term_puts("\nEXTRA Pilot control keys:\n");
			term_puts("[kb]u                      : increase P parameter\n");
			term_puts("[kb]j                      : decrease P parameter\n");
			term_puts("[kb]i                      : increase P1 parameter\n");
			term_puts("[kb]k                      : decrease P1 parameter\n");
			term_puts("[kb]o                      : increase P2 parameter\n");
			term_puts("[kb]l                      : decrease P2 parameter\n");
			term_puts("\n|-------------------------------------------------|\n\n");
			break;
		default:
			break;
		}
		previous_mode = userstate->mode;
	}
}
