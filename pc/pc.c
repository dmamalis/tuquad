/*  
 *  PC side
 *
 *	Communication - Kimon Tsitsikas
 *	GUI - Nikos Larisis
 *	Data logging - Dimitris Mamalis
 *
 *--------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>
#include <sys/timex.h>

#include "./lib/common.h"
#include "./lib/pc.h"
#include "./lib/joystick.h"
#include "./lib/control_pc.h"
#include "./lib/print_state.h"

/*----------------------------------------------------------------
 * main -- execute terminal
 *----------------------------------------------------------------
 */
int main(int argc, char **argv)
{
	
	int i, j,v=10,z, int_c, bad_input = 0,dl_flag = 0, rcv = 0;
	unsigned char mode2qr, lift2qr, roll2qr, pitch2qr, yaw2qr, rem2qr, p2qr, p12qr, p22qr;
	char c;
	FILE *dl_file;
	int new_values[9];
	int cor_values[9];
	unsigned char crc_values[9];
	unsigned char crc_values2[9];
	unsigned int sensor = 0;
	int cnt_dl_file = 1;
	int no_of_dlpacket = 0;
	long long int system_time=0;
	int scaling;

	// For timing measurments 
	struct ntptimeval now;
	static long umark;
	long dif;
	int flag_time_start = 0;
	int flag_time_end = 1;

	// For Real-Time plotting
	FILE *pipe;
    FILE *p = NULL;
    char *file = "QRdata.dat";
	
    pipe = popen("gnuplot", "w"); 								//open up a pipe to gnuplot
	fprintf(pipe,"set xlabel \"Time\";"); 						//setting up the environment
	fprintf(pipe,"set title \"QR Real-Time data\";"); 
	fprintf(pipe,"set grid;");
	fprintf(pipe,"set key top left;");
	fprintf(pipe,"set xrange [0:500];"); 
	fprintf(pipe,"set yrange [0:1207];"); 
	
	p = fopen(file, "w");
	
	// Allocating memory for structs 
	frameflag = FRAMESTART;
	Flightvals *userstate = (Flightvals *)malloc(sizeof(Flightvals));
	Flightvals *kb_state = (Flightvals *)malloc(sizeof(Flightvals));
	Flightvals *js_state = (Flightvals *)malloc(sizeof(Flightvals));
	
	// For Data logging 
	dl_file = fopen("datalog.txt","wb");
	
	// Initializing vector values 
	for (i=0; i<9; i++){
		new_values[i] = 0;
		cor_values[i] = 0;
		crc_values[i] = 0;
		crc_values2[i] = 0;
	}

	// Initializing structs
	userstate->mode = SAFEMODE;
	userstate->lift = 0;
	userstate->roll = 0;
	userstate->pitch = 0;
	userstate->yaw = 0;
	userstate->p  = 0;
	userstate->p1  = 0;
	userstate->p2  = 0;
	
	js_state->mode = SAFEMODE;
	js_state->lift = 0;
	js_state->roll = 0;
	js_state->pitch = 0;
	js_state->yaw = 0;
	js_state->p  = 0;
	js_state->p1  = 0;
	js_state->p2  = 0;

	kb_state->mode = SAFEMODE;
	kb_state->lift = 0;
	kb_state->roll = 0;
	kb_state->pitch = 0;
	kb_state->yaw = 0;
	kb_state->p  = 0;
	kb_state->p1  = 0;
	kb_state->p2  = 0;

	js_init();
	// Check input 
	if (argc == 1) 
    		serial_device = 1; 
	
	else if (argc == 2) 
	{
		if (strncmp(argv[1],"serial",3) == 0)
			serial_device = 0;

		else if (strncmp(argv[1],"usb",3) == 0)
			serial_device = 1;

		else if (strncmp(argv[1],"wifi",3) == 0)
			serial_device = 2;

		else 
			bad_input = 1;	
	} 
	else 
		bad_input = 1;

	if (bad_input == 1) 
	{
		fprintf(stderr,"Usage: ./term [serial|usb|wifi]\n"); 
		return -1;
	}

	term_puts("\nTerminal program (Embedded Software Lab), ");

	term_initio();
	rs232_open();

	// Welcoming Message 
	term_puts("\n|-------------------------------------------------|");
	term_puts("\n|       Embedded Real-Time Systems Lab (IN4073)   |");
	term_puts("\n|                                                 |");
	term_puts("\n| Group - B :      Arnold de Jager    (1398938)   |");
	term_puts("\n|                  Nikos Larisis      (4185951)   |");
	term_puts("\n|                  Dimitris Mamalis   (4183096)   |");	
	term_puts("\n|                  Kimon Tsitsikas    (4183363)   |");
	term_puts("\n|                                                 |");
	term_puts("\n|-------------------------------------------------|\n");

	// discard any incoming text 
	while ((c = rs232_getchar_nb()) != -1)
		fputc(c,stderr);
	
	// Check whether the joystick is not in neutral position 
	joystick(js_state);
	
	if (js_state->lift !=0 || js_state->roll !=0 || js_state->pitch !=0 || js_state->yaw !=0 ){
		printf("\n///////////////////////////////////////////////////\n");
		printf("////////         W A R N I N G            /////////\n");
		printf("//       Joystick not in neutral position        //\n");
		printf("//              System exiting...                //\n");
		printf("///////////////////////////////////////////////////");
		printf("\n\n<exit>\n");
		term_exitio();
		rs232_close();
		fclose(dl_file);
	  	free (kb_state);
	  	free (js_state);
		free(userstate);
		return 0;
	}

	for (;;) 
	{
		joystick(js_state);
		
		// Measuring the system response - PC side	
		if (kb_state->mode == 2)
		if(!flag_time_start){
			ntp_gettime(&now);
			umark = now.time.tv_usec;
			flag_time_start = 1;
			flag_time_end = 0;
		} 
		
		if ((c = term_getchar_nb()) != -1) 
		{
			keyboard(c,kb_state,cor_values);	
		}	

		control_pc(userstate,kb_state,js_state,cor_values);

		// Invoking correspondent GUI messages
		print_state(userstate);

 		mode2qr = (unsigned char) userstate->mode;
 		lift2qr = (unsigned char ) userstate->lift;
 		roll2qr = (unsigned char ) userstate->roll;
 		pitch2qr = (unsigned char ) userstate->pitch;
 		yaw2qr = (unsigned char ) userstate->yaw;
		p2qr = (unsigned char ) userstate->p;
 		p12qr = (unsigned char ) userstate->p1;
 		p22qr = (unsigned char ) userstate->p2;
 		
 		// P C  ->  F P G A  communication
		rs232_putchar(frameflag);
		if (frameflag == ESCSTART)
			break;		
 		rs232_putchar(mode2qr);
 		rs232_putchar(lift2qr);
 		rs232_putchar(roll2qr);
 		rs232_putchar(pitch2qr);
 		rs232_putchar(yaw2qr);	
		rs232_putchar(p2qr);
 		rs232_putchar(p12qr);
 		rs232_putchar(p22qr);
		crc_values[0] = mode2qr;
		crc_values[1] = lift2qr; 
		crc_values[2] = roll2qr;
		crc_values[3] = pitch2qr; 
		crc_values[4] = yaw2qr; 
		crc_values[5] = p2qr;
		crc_values[6] = p12qr;
		crc_values[7] = p22qr; 
		crc_values[8] = 0;
		// Computing the CRC remainder for the PC -> FPGA communication	
		rem2qr =  crc (crc_values, 1);
		rs232_putchar(rem2qr);

		// Measuring the response time - PC side 		
		if (kb_state->mode == 2)
		if(!flag_time_end && (flag_time_start == 1)){
			ntp_gettime(&now);
			dif = now.time.tv_usec - umark;
           	        printf("%ld microseconds\n",dif);
			flag_time_end = 1;
		} 
		
		// P C  <-  F P G A  communication	
		// Receiving the mode,rotor and controller values from the fpga one time per 4 loops
		if (rcv = 4){
			for (i=0; i<10; i++) {
				while ((int_c = rs232_getchar_nb()) == -1) {}
				if ((v != 9) && (int_c !=FRAMESTART)){
					new_values[v] = int_c;
					crc_values[v] = (unsigned char) new_values[v];
					v++; 
				}
				if (int_c == FRAMESTART) {
					v = 0;	
				}
				if (v == 9){
				    // Performing CRC check for the received values from FPGA -> PC
					if(crc (crc_values, 0)){
						cor_values[0] = new_values[0];
						kb_state->mode = cor_values[0]; 
						cor_values[1] = (new_values[1]<<3);
						cor_values[2] = (new_values[2]<<3);
						cor_values[3] = (new_values[3]<<3);
						cor_values[4] = (new_values[4]<<3);
						cor_values[5] = new_values[5]-102;
						cor_values[6] = new_values[6]-102;
						cor_values[7] = new_values[7]-102;
						cor_values[8] = new_values[8];
					}

					// Uncomment to see raw data
					//printf("%d %d %d %d %d %d %d %d \n", cor_values[0], cor_values[1], cor_values[2], cor_values[3], cor_values[4], cor_values[5], cor_values[6], cor_values[7] );

				}
			
			}
			rcv = 0;
		}
		else 
			rcv++;
			
		/* Real-Time plotting 
		 * Description - We retain a file in which the values from received the FPGA are stored/appended at the end of it.
		 * The pipe/flow connecting the executable and the file is flushed upon passing of each segment of values
		 * so that the values are truly stored. Then a new pipe will connect the "updated" file and the executable
		 * and the gnuplot application will print on a graphic the stored values. By this, a real-time plot is obtained. 
		 * @author 	   - Nikos Larisis
		 */
		
		p = fopen(file, "a+");
	
		for(i=0; i<8; i++){
			fprintf(p, "%d ",cor_values[i]);
		}
		fprintf(p, "\n");
			
		fflush(p);
		fclose(p);
	
		fprintf(pipe,"plot \"%s\" using 1 with lines lc rgb \"white\" title 'Mode - %d', \"%s\" using 2 with lines title 'ae[0] - %d', \"%s\" using 3 with lines title 'ae[1] - %d', \"%s\" using 4 with lines title 'ae[2] - %d', \"%s\" using 5 with lines title 'ae[3] - %d', \"%s\" using 6 with lines lc rgb \"white\" title 'P - %d', \"%s\" using 7 with lines lc rgb \"white\" title 'P1 - %d', \"%s\" using 8 with lines lc rgb \"white\" title 'P2 - %d'; ",file,cor_values[0],file,cor_values[1],file,cor_values[2],file,cor_values[3],file,cor_values[4],file,cor_values[5],file,cor_values[6],file,cor_values[7]);
	
		fflush(pipe); 

		fprintf(pipe, "refresh \n");
		fflush(pipe);	
	}
	
	// Data logging file completion
	for (j=0;j<120;j++) {
		for (i=0; i<10; i++) {
			while ((int_c = rs232_getchar_nb()) == -1) {}
			
			if ((v != 9) && (int_c != ESCSTART)){
				new_values[v] = int_c;
				crc_values2[v] = (unsigned char) new_values[v];
				v++; 
			}
			if (int_c == ESCSTART) {
				v = 0;	
			}
			if (v == 9){
			        // Performing CRC check for the received values from FPGA -> PC
				if(crc (crc_values2, 0)){
					cor_values[0] = new_values[0];
					cor_values[1] = new_values[1];
					cor_values[2] = new_values[2];
					cor_values[3] = new_values[3];
					cor_values[4] = new_values[4];
				}
				// Packet with the system time and the system mode
				if (no_of_dlpacket == 0) {
					system_time  = (unsigned long long int) (cor_values[0] << 24);
					system_time |= (unsigned long long int) (cor_values[1] << 16);
					system_time |= (unsigned long long int) (cor_values[2] <<  8);
					system_time |= (unsigned long long int) (cor_values[3]      );
					fprintf(dl_file,"%lld %d ", system_time, cor_values[4]);
					if (system_time == 0)
						sensor = 0;
				}
				if (no_of_dlpacket == 1) {
					sensor |= (unsigned int) (cor_values[2] <<  8);
					sensor |= (unsigned int) (cor_values[3]      );
					fprintf(dl_file,"%d %d %d\n", cor_values[0], sensor, cor_values[4]);
					no_of_dlpacket = -1;
				}
				no_of_dlpacket++;
			}
		
		}
	}
	printf("\n<exit>\n");
	term_exitio();
	rs232_close();
	fclose(dl_file);
  	free (kb_state);
  	free (js_state);
	free(userstate);
	return 0;
}
