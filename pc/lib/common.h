#ifndef COMMON_H_
#define COMMON_H_
#define FRAMESTART	255
#define ESCSTART	254
#define ENDSTART	253
#define DL_CAPACITY	600

typedef struct vals{
 	int mode;
 	int lift;
 	int roll;
 	int pitch;
 	int yaw;
 	int p;
 	int p1;
 	int p2;
}Flightvals;

int frameflag;// = FRAMESTART;
//int calib_ready = 0;

enum qrmodes {SAFEMODE , PANICMODE, MANUALMODE, CALIBRATIONMODE, YAWMODE, FULLMODE};
enum qrmodes qrmode;


//void control_pc(Flightvals *userstate);
#endif /*COMMON_H_*/
