/*  
 *  Joystick driver
 *
 *	Some mods regarding sensitivity & mapping (Dimitris Mamalis)
 *	
 *
 *--------------------------------------------------------------------------
 */

#include <stdio.h>
#include <fcntl.h>
#include "./lib/joystick.h"
#include "./lib/common.h"
/*--------------------------------------------------------------------------
 * initialize joystick
 *--------------------------------------------------------------------------
 */
void js_init(void)
{
	int i;

	button[0] = 0;
	for (i = 0; i < 3; i++) 
		axis[i]=0;
	axis[3] = 32767;

	no_js = 1;
	if ((fd = open(JS_DEV, O_RDONLY)) < 0) {
		fprintf(stderr,"warning: error opening JS\n");
		perror("jstest");
		no_js = 1;
		return;
	}
	fcntl(fd, F_SETFL, O_NONBLOCK);
	read(fd, &js, sizeof(struct js_event));
	read(fd, &js, sizeof(struct js_event));
	read(fd, &js, sizeof(struct js_event));	
	no_js = 0;
}


void joystick(Flightvals *js_state)
{
		/* check for JS events
		 */
		 while (! no_js && read(fd,&js,sizeof(struct js_event)) == 
		       sizeof(struct js_event))  {
			switch(js.type & ~JS_EVENT_INIT) {
				case JS_EVENT_BUTTON:
					button[js.number] = js.value;
					break;
				case JS_EVENT_AXIS:
					axis[js.number] = js.value;
					break;
			}
		}
			
		if (button[0] == 1)
			frameflag = ESCSTART;
		js_state->roll = axis[0] / (10 * JS_SENSITIVITY);
		js_state->pitch = axis[1]/ (10 * JS_SENSITIVITY);
		js_state->yaw = axis[2] / (10 * JS_SENSITIVITY);
		js_state->lift = -(axis[3] - 32767) / (10*JS_SENSITIVITY);
}
