%% Filter Test

clear all
close all

%% LOAD FILE

load logfile.dat

% PLOT for variable kalman control parameters
PLOT = 0;

% Range for bias averaging
L = 3072;

% Kalman filter parameters
c2 = 2^18;
c1 = 2^9;

for j = 1:1     % Loop for different C2's
    
    %c2 = 2^(18-(j*1));
    %c1 = 2^(12-(j*1));

%% VARIABLES

N = int32(2^9);        %       #define N 512

sum_sax_v = int32(0); sum_say_v = int32(0); sum_saz_v = int32(0);       % initialize
sum_sp_v = int32(0); sum_sq_v = int32(0); sum_sr_v = int32(0);

pos = int32(1);        %       int position = 0;

P2PHI = int32(3281);   %       #define P2PHI 3281
Q2THE = int32(3091);   %       #define Q2THE 3091

p_b = int32(12298);        %       initialize
q_b = int32(21416);        %       initialize

phi = int32(0);        %       initialize
the = int32(0);        %       initialize

p = int32(0);          %       initialize
q = int32(0);          %       initialize

C_1 = int32(c1); C_2 = int32(c2);   %   #define ...

%% CALIBRATE

done = 0;
while ~done
    
    % Read sensor value
    sax_v(pos) = int32(logfile(pos,3));
    say_v(pos) = int32(logfile(pos,2));
    saz_v(pos) = int32(logfile(pos,4));
    sp_v(pos) = int32(logfile(pos,5));
    sq_v(pos) = int32(logfile(pos,6));
    sr_v(pos) = int32(logfile(pos,7));
    
    % Sum p, q (for debugging)
    if pos == 1
        SUM_p = sp_v; SUM_q = sq_v;
    else    
        SUM_p(pos) = SUM_p(pos-1) + sp_v(pos);
        SUM_q(pos) = SUM_q(pos-1) + sq_v(pos);
    end 
    
    if pos == N
       
        for i = 1:N-1
            
            % Sum iteratively
            sum_sax_v = sum_sax_v + sax_v(i);
            sum_say_v = sum_say_v + say_v(i);
            sum_saz_v = sum_saz_v + saz_v(i);
            sum_sp_v = sum_sp_v + sp_v(i);
            sum_sq_v = sum_sq_v + sq_v(i);
            sum_sr_v = sum_sr_v + sr_v(i);
            
            % Sum for bias approximation            
            if i == N/2^6
                Sum_bp_1 = sum_sp_v;
                Sum_bq_1 = sum_sq_v;
                Sum_bp_2 = 0;
                Sum_bq_2 = 0;
                n = i;
            end
            
        end
        
        % Shift and average
        sax0 = sum_sax_v*(2^14/N);
        say0 = sum_say_v*(2^14/N);
        saz0 = sum_saz_v*(2^14/N);
        sp0 = sum_sp_v*(2^14/N);
        sq0 = sum_sq_v*(2^14/N);
        sr0 = sum_sr_v*(2^14/N);
        
        % Approximate bias (divide by n samples)
        %p_b = (sp0 - Sum_bp_1*(2^14/n))/(N/2)*2^14;
        %q_b = (sq0 - Sum_bq_1*(2^14/n))/(N/2)*2^14;
%         p_b = int32(0.763*2^14);
%         q_b = int32(1.79957*2^14);
        
        % Flag done
        done = 1;
        
    end
    
    pos = pos + 1;
    
end

%% FULL MODE

done = 0; i = 1;
while ~done
    
    % Calibrate samples
    zx = (logfile(i,3)*2^14 - sax0);
    zy = (logfile(i,2)*2^14 - say0); 
    zz = (logfile(i,4)*2^14 - saz0);
    zp = (logfile(i,5)*2^14 - sp0);
    zq = (logfile(i,6)*2^14 - sq0);
    zr = (logfile(i,7)*2^14 - sr0);
    
    % TEST
%     zy = 28*2^14;
%     zp = 16*2^14;
    
    % Kalman filtering p, phi
    p   = zp - p_b;
    phi = phi + p*P2PHI/2^14;
    phi = phi - (phi - zy)/C_1;
    p_b = (p_b*2^6 + (phi - zy)*2^6/C_2)/2^6;
    
    % Kalman filtering q, theta
    q = zq - q_b;
    the = the + q*Q2THE/2^14;
    the = the - (the - zx)/C_1;
    q_b = (q_b*2^6 + (the - zx)*2^6/C_2)/2^6;
    
    % Store p, q, phi, theta, p_b, q_b
    P(i) = p; PHI(i) = phi; P_B(i) = p_b;
    Q(i) = q; THE(i) = the; Q_B(i) = q_b;
    
    % Sum p, q (for debugging)
    if i == 1
        Sp = zp; Sq = zq;
        SP = p; SQ = q;
        Sphi = zp*P2PHI/2^14;
        SPHI = zp*P2PHI/2^14;
        Sthe = zq*Q2THE/2^14;
        P_bias = zp*P2PHI/2^14;
    else
        % Sum of fixed bias rates
        Sp(i) = Sp(i-1) + zp - int32(0.7528*2^14);
        Sq(i) = Sq(i-1) + zq - int32(1.79957*2^14);
        
        % Sum of dynamic bias rates
        SP(i) = SP(i-1) + p;
        SQ(i) = SQ(i-1) + q;
        
        % Sum of assumed actual rates (based on fixed bias)
        Sphi(i) = Sphi(i-1) + (zp - 0.7528*2^14)*P2PHI/2^14;
        Sthe(i) = Sthe(i-1) + (zq - 1.79812*2^14)*Q2THE/2^14;
        
        % Sum of uncorrected rates
        SPHI(i) = SPHI(i-1) + zp*P2PHI/2^14;
        
        % Summed rate approximation based on bias
        P_bias(i) = P_bias(i-1) + int32(0.763*P2PHI);
    end
    
    % Flag if end of file
    if i == length(logfile)
        done = 1;
    end
    
    % Increment
    i = i + 1;
    
end

plot_bias(:,j) = P_B; 

end

%% PLOT RESULTS

% Define X values
x = 1:length(logfile);
x_n = 1:length(SUM_p);

if PLOT
    % PLOT FILTERED BIAS
    figure()
    plot(x,(0.7811*ones(1,length(P_B))),'b',...
         x,double(plot_bias(:,1))/2^14,'g',...
         x,double(plot_bias(:,2))/2^14,'r',...
         x,double(plot_bias(:,3))/2^14,'c',...
         x,(mean(double(plot_bias(1024:L,1)))/2^14)*ones(1,length(P_B)),'g',...
         x,(mean(double(plot_bias(1024:L,2)))/2^14)*ones(1,length(P_B)),'r',...
         x,(mean(double(plot_bias(1024:L,3)))/2^14)*ones(1,length(P_B)),'c')
    title('CALIBRATION')
    legend('ref','2^1^1','2^1^0','2^0^9')
    grid on
end

% PLOT ASSUMED ACTUAL ANGLES - correct?
figure()
plot(x,(THE)/2^14,'b',x,Sthe/2^14,'r')
title('THETA ANGLE COMPARISON')
legend('PITCH ANGLE','FIXED AVERAGE BIAS')

% PLOT FILTERED ANGLES
figure()
% plot(x,PHI,'b',x,THE,'r')
plot(x,(PHI)/2^14,'b',x,Sphi/2^14,'r')
title('PHI ANGLE COMPARISON')
legend('ROLL ANGLE','FIXED AVERAGE BIAS')

% PLOT FILTERED BIAS
figure()
% plot(x,P_B,'b',x,Q_B,'r')
plot(x,double(P_B)/2^14,'b',x,0.753,'r')
%plot(x,double(P_B)/2^14)
%axis([0 10000 0.75061035 0.75061036])
title('ERROR')
legend('DYNAMIC BIAS','FIXED AVERAGE BIAS')

% % COMPARE IDEAL RATE WITH ACTUAL CALCULATED RATE
% figure()
% plot(x,Sp,'g',x,SP,'b')
% title('P RATE')
% 
% figure()
% plot(x,Sq,'g',x,SQ,'r')
% title('Q RATE')

% % SUMMED RATES
% figure()
% plot(x,SPHI/2^14,'g',x,P_bias/2^14,'c')
% title('BIAS FROM ANGLE GRADIENT')
