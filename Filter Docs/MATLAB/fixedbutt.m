%% Sensor Filtering
%
%   Filter testing
 
clear all
close all

order = 1;

%% GLOBAL VARIBLES
% #define

F_s = 1267;     % Sampling frequency in Hz
F_c = 20;       % Cut-off frequency

%% FILE OPEN

load logfile.dat
data = logfile;

frame = [1, length(data)];
t = frame(1):frame(2);

% F_s = (length(data)*1e6)/(data(end,1)-data(1,1));

%% Coefficients if butterworth filter is applied

[a,b] = butter(order,(F_c/F_s)*2);
b(2) = -b(2);

A = int32(a.*2^14);
B = int32(b.*2^10);

x0 = int32(data(frame(1),7)) - 408;

switch order
    
    case 1      % Order 1
        
        a0 = A(1); a1 = A(2); b1 = B(2);
        x1 = x0*2^10;
        y1 = x0*2^14;
        
    case 2      % Order 2

        a0 = A(1); a1 = A(2); b1 = B(2);
        a2 = a0; b2 = B(3);
        x1 = x0*2^10;
        x2 = x0*2^10;
        y1 = x0*2^14;
        y2 = x0*2^14;
        
end

%% LOOP MAIN

for i = 1:(frame(2)-frame(1)+1)
    
    % Read 'sensor' value
    srate = int32(data(i+frame(1)-1,7)) - 408;
    plotdata(i) = srate;
    
    % Butterworth filter order
    switch order
        
        case 1      % First order
            
            x0 = srate*2^10;
            y0 = (a0*x0) + (a1*x1) + (b1*y1);
            %y0 = y0/2^10;
            
        case 2      % Second order
            
            x0 = srate*2^10;
            y0 = (a0*x0) + (a1*x1) + (a2*x2) + (b1*y1) - (b2*y2);
            %y0 = y0/2^10;
            
    end
    
    % For plotting
    y(i) = double(y0)/2^24;
    
    % Shift variables
    if order == 2
        x2 = x1;
        y2 = y1;
    end
    
    x1 = x0;
    y1 = y0/2^10;
    
end

%% Plot results

y = y';
figure()
title('BUTTERWORTH FILTER');
hold on
plot(t,plotdata','b',t,y,'r')
hold off
