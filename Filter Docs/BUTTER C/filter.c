#include <stdio.h>

// Filter coefficients for 1st order Butterworth LP and for Fs=1267Hz and Fc=20Hz
#define a0 775		// a coefficients 2^14
#define a1 775
#define b0 1024		// b coefficients 2^10
#define b1 927

/*#define a0 99		// a coefficients 2^12
#define a1 99
#define b0 4096		// b coefficients 2^12
#define b1 3898 */

int filter_1stBLP(int z_i);
int order = 24;

FILE *fp = NULL;
FILE *p = NULL;
FILE *q = NULL;
char *file = "output.txt";
char *filea = "check.txt";

// Filter 1st order Butterworth LP variables
int x0 = 0;
int x1 = 0;
int z0 = 0;
int z1 = 0;

int zr = 0;

// Time
//clock_t t1 = 0;
//clock_t t2 = 0;

int flag_first_filter = 1;

int sr0 = 409600;	// 400 [^10]

int main() 
{
	printf("Filtering...\n");
	
	int sax,say,saz,sp,sq,sr,fr=0;
	
	if((fp= fopen("sensors_data_3.m","r")) == NULL) {
    		printf("Cannot open file.\n");
 	}
 	
 	if((p = fopen(file, "w")) == NULL) {
    		printf("Cannot open file.\n");
 	}

 	if((q = fopen(filea, "w")) == NULL) {
    		printf("Cannot open file.\n");
 	}
	
	while (fscanf(fp, "%d%d%d%d%d%d", &sax,&say,&saz,&sp,&sq,&sr) !=EOF){ 
  		//printf("%d %d %d %d %d %d\n", sax,say,saz,sp,sq,sr); 
  		
  		p = fopen(file,"a+");
		q = fopen(filea,"a+");
  		
		//t1 = clock();

		zr = ((sr << 10) -sr0);

  		fr = filter_1stBLP(zr);  	

		//t2 = clock();		
	
		fprintf( p,"%d\n", fr);  
		
		fflush(p);
		fflush(q);
		fclose(p);
		fclose(q); 		
  	}	
}

/*-------------------------------------------------------------------
 * filter_1stBLP -- 2nd order BW filter 10Hz with fixed point scheme
 *-------------------------------------------------------------------
 */
 int filter_1stBLP(int z_i){
	
	x0 = z_i;		// +/- 10 [^10]
	
	if(flag_first_filter){
		x1 = x0;		// +/- 10 [^10]
		z1 = x0 << 4;		// +/- 10 [^14]
		flag_first_filter = 0;
	}
	
	// Multiplication implementation
	
	fprintf(q," a0-%d \ta1-%d \t\tb1-%d \t\n",a0,a1,b1);	
	fprintf(q," x0-%d \t\tx1-%d \t\tz1-%d \t\n",x0,x1,z1);

	z0 = (a0*x0) + (a1*x1) + (b1*z1);	// z0 [^24] = (a0*x0) [^14]*[^10] + (a1*x1) [^14]*[^10] + (b1*z1)[^10]*[^14]
	z0 = z0 >> 10;				// z0 [^24] >> 10 --> z0 = [^14]

	fprintf(q,"z0-%d\t",z0>>24);
	
	// Shifting implementation
	x1 = x0; z1 = z0;	

	fprintf(q,"z1-%d\n\n",z1>>14);	
	
	return z0;		// z0 = [^14] - currently z0 >> 14
 
 }
