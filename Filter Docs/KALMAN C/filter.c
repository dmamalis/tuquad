#include <stdio.h>

#define P2PHI 205//3281
#define Q2THE 193//3091

int p_b = 0;//12501;	// 0.763 << 14	
int phi = 0;
int prate = 0;

int q_b = 0;//29484;	// 1.79957 << 14
int the = 0;
int qrate = 0;

/*int sax0 = 522 << 10;
int say0 = 533 << 10;
int saz0 = 429 << 10;
int sp0 = 414 << 10;
int sq0 = 452 << 10;
int sr0 = 408 << 10;	// */

int sax,say,saz,sp,sq,sr;
int zx,zy,zz,zp,zq,zr;

void filter_kalman(void);

FILE *fp = NULL;
FILE *p = NULL;
FILE *q = NULL;
char *file = "output.txt";
char *filea = "check.txt";
int pphi;
int pthe;

int sax0 = 8475552;	// [^14]
int say0 = 8569216;	// [^14]
int sp0 = 6792816;	// [^14]
int sq0 = 7409008;	// [^14]
int sr0 = 6712592;	// [^14]

int main() 
{
	printf("Filtering...\n");
	
	if((fp= fopen("sensors_data_3.m","r")) == NULL) {
    		printf("Cannot open file.\n");
 	}
 	
 	if((p = fopen(file, "w")) == NULL) {
    		printf("Cannot open file.\n");
 	}

 	if((q = fopen(filea, "w")) == NULL) {
    		printf("Cannot open file.\n");
 	}
	
	while (fscanf(fp, "%d%d%d%d%d%d", &sax,&say,&saz,&sp,&sq,&sr) !=EOF){ 
  		//printf("%d %d %d %d %d %d\n", sax,say,saz,sp,sq,sr); 
  		
  		p = fopen(file,"a+");
		q = fopen(filea,"a+");
  		
		filter_kalman();
		//pphi = phi >> 14;
		//pthe = the >> 14;  		
		fprintf(p,"%d\t%d\n",phi,the);  
		
		fflush(p);
		fflush(q);
		fclose(p);
		fclose(q); 		
  	}	
}

/*-------------------------------------------------------------------
 * filter_kalman -- Kalman filter with fixed point implementation
 *-------------------------------------------------------------------
 */
void filter_kalman (void){

	// Calibrate Samples
	zx = ( (sax << 14) - sax0);
	zy = ( (say << 14) - say0);
	zp = ( (sp << 14) - sp0);
	zq = ( (sq << 14) - sq0);
	zr = ( (sr << 14) - sr0);

	// Kalman filtering p, phi
	prate  = zp - p_b;
	phi = phi + ( (prate*P2PHI) >> 10 );		// [^14]*[^10], so >> 10
	phi = phi - ( (phi - zy) >> 9 );		// CP_1 = 512 --> 9 bit shift
	p_b = ( (p_b << 6) + ((phi - zy) >> 12) ) >> 6;	// CP_2 = 262144 --> 18 bit shift, [^6] magnification, so >> 12

	// Kalman filtering q, theta
	qrate  = zq - q_b;
	the = the + ( (qrate*Q2THE) >> 10 );		// [^14]*[^10], so >> 10
	the = the - ( (the - zx) >> 9 );		// CQ_1 = 512 --> 9 bit shift
	q_b = ( (q_b << 6) + ((the - zx) >> 12) ) >> 6;	// CQ_2 = 262144 --> 18 bit shift, [^6] magnification, so >> 12
}
